package main

import (
	"fmt"
)

func main() {
	fmt.Println("GCD 12, 18", gcd(12, 18))
	fmt.Println("GCD 16, 20", gcd(16, 20))
	fmt.Println("GCD 120, 900", gcd(120, 900))
	fmt.Println("GCD 105, 26", gcd(105, 26))
}

func gcd(a, b int) int {

	if a == 0 {
		return b
	}

	if b == 0 {
		return a
	}

	// while does not exist in go
	for b != 0 {
		h := a % b
		a = b
		b = h
	}

	return a
}
