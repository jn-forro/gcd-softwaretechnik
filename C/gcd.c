#include <stdio.h>
#include <stdlib.h>

int main() {
    printf("GCD ( %d , %d ) is %d.", 12, 18, gcd(12, 18));
    printf("GCD ( %d , %d ) is %d.", 16, 20, gcd(16, 20));
    printf("GCD ( %d , %d ) is %d.", 120, 900, gcd(120, 900));
    printf("GCD ( %d , %d ) is %d.", 105, 26, gcd(105, 26));

    return 0;
}

int gcd(int a, int b) {

    if(a == 0 )
        return b; 
    
    if(b == 0)
        return a; 
    
    while (b != 0)
    {
        /* code */
        const int h =  a % b;
        a = b;
        b = h; 
    }

    return a;
    
}
