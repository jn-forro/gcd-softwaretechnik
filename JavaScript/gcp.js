function gcd (a, b){

    if(a === 0 || a === undefined) 
        return b;

    if(b === 0 || b === undefined)
        return a;

    while (b !== 0) {
        const h = a % b;
        a = b;
        b = h;
    } 

    return a;
}

function oneLineGCD(a,b){
    
    return (a % b === 0) ?
        b : oneLineGCD(b, a % b); 
}
 
console.log( gcd(12, 18) )
console.log( gcd(16, 20) )
console.log( gcd(120, 900) )
console.log( gcd(105, 26) )

console.log( oneLineGCD(12, 18) )
console.log( oneLineGCD(16, 20) )
console.log( oneLineGCD(120, 900) )
console.log( oneLineGCD(105, 26) )